import pytest

from gnunet.crypto import EcdsaPublicKey

def test___repr__():
    k = EcdsaPublicKey(b"abcd"*(256//8//4))
    assert repr(k).startswith('gnunet.crypto.EcdsaPublicKey(')

def test___init__string():
    s = "C5H66P31C9HM8OB2CDI62OJ3CHGM4OR4C5H66P31C9HM8OB2CDI0"
    k = EcdsaPublicKey(s)

def test___init__bytes():
    k = EcdsaPublicKey(b"abcd"*(256//8//4))

def test___init_EcdsaPublicKey():
    k1 = EcdsaPublicKey(b"abcd"*(256//8//4))
    k2 = EcdsaPublicKey(k1)
    assert k1._data == k2._data

def test___init__string_wrong_size():
    with pytest.raises(ValueError):
        k = EcdsaPublicKey("abcd")
    with pytest.raises(ValueError):
        k = EcdsaPublicKey("abcd"*256)

def test___init__bytes_wrong_size():
    with pytest.raises(ValueError):
        k = EcdsaPublicKey(b"abcd")
    with pytest.raises(ValueError):
        k = EcdsaPublicKey(b"abcd"*256)
