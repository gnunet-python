import pytest

from gnunet import HashCode

def test___repr__():
    k = HashCode(b"abcd"*(512//8//4))
    assert repr(k).startswith('gnunet.HashCode(')

def test___init__string():
    s = ("C5H66P31C9HM8OB2CDI62OJ3CHGM4OR4C5H66P31C9HM8OB2CDI6"
         "2OJ3CHGM4OR4C5H66P31C9HM8OB2CDI62OJ3CHGM4OR4C5H66P0")
    k = HashCode(s)

def test___init__bytes():
    k = HashCode(b"abcd"*(512//8//4))

def test___init_HashCode():
    k1 = HashCode(b"abcd"*(512//8//4))
    k2 = HashCode(k1)
    assert k1._data == k2._data

def test___init__string_wrong_size():
    with pytest.raises(ValueError):
        k = HashCode("abcd")
    with pytest.raises(ValueError):
        k = HashCode("abcd"*512)

def test___init__bytes_wrong_size():
    with pytest.raises(ValueError):
        k = HashCode(b"abcd")
    with pytest.raises(ValueError):
        k = HashCode(b"abcd"*512)
