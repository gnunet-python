import pytest

from gnunet import strings

import locale
locale.setlocale(locale.LC_ALL, "en_US.UTF-8")

test_dates = (
    ("Sat Jan 07 19:54:12 2008", (2008, 1, 7, 19, 54, 12)),
    ("Thu 27 Mar 2005 11:05:25 PM CET", (2005, 3, 27, 23, 5, 25)),
    # FIXME: need test-case for %Ec
    ("2019-02-23 16:07:22", (2019, 2, 23, 16, 7, 22)),
    ("1998-08-17 14:33", (1998, 8, 17, 14, 33, 0)),
    ("12/15/2007", (2007, 12, 15, 0, 0, 0)),
    # FIXME: need test-case for %Ex
    ("2019-02-26", (2019, 2, 26, 0, 0, 0)),
    ("2014-03-23", (2014, 3, 23, 0, 0, 0)),
    ("2020", (2020, 1, 1, 0, 0, 0))
    )

@pytest.mark.parametrize("datestr,expected", test_dates)
def test_string_to_absolute_time(datestr, expected):
    dt = strings.string_to_absolute_time(datestr)
    assert dt.timetuple()[:6] == expected


def test_string_to_absolute_time_end_of_time():
    assert strings.string_to_absolute_time("end of time") is None


def test_string_to_absolute_invalid():
    with pytest.raises(ValueError) as excinfo:
        strings.string_to_absolute_time("asdfgh")
    assert str(excinfo.value).startswith("asdfgh is not a properly formatted")


def test_data_to_string():
    assert strings.data_to_string(b"") == ""
    assert strings.data_to_string(b"dfgzu") == "CHJ6EUJL"


def test_string_to_data():
    assert strings.string_to_data("") == b""
    assert strings.string_to_data("CHJ6EUJL") == b"dfgzu"
    with pytest.raises(ValueError) as excinfo:
        strings.string_to_data("asdfgh")
    assert "is not a valid data-encoding string" in str(excinfo.value)
    with pytest.raises(ValueError) as excinfo:
        strings.string_to_data(["asdfgh"])
    assert "is not a valid data-encoding string" in str(excinfo.value)
