from . import _Key

class EcdsaPublicKey(_Key):
    __bits__ = 256
