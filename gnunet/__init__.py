from . import strings


class GNUNetDaemonError(Exception):
    pass


class _Key:
    def __init__(self, arg):
        if isinstance(arg, type(self)):
            self._data = arg._data
        elif isinstance(arg, str):
            self._data = strings.string_to_data(arg)
        else:
            try:
                self._data = bytearray(arg)
            except TypeError as e:
                # HACK: replace the exception message
                e.args = ("'arg' must be a %s, a string or an array "
                          "of bytes. Not a '%s'." %
                          (type(self).__name__, type(arg).__name__),)
                raise

        if len(self._data) * 8 != self.__bits__:
            raise ValueError("'arg' must be a %s bit hash. Got %s bits."
                             % (self.__bits__, len(self._data) * 8))

    def __str__(self):
        return strings.data_to_string(self._data)

    def __repr__(self):
        cls = self.__class__
        return "%s.%s(%r)" % (cls.__module__, cls.__name__, str(self))


class HashCode(_Key):
    __bits__ = 512
